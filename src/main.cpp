#include <Arduino.h>
#include <WiFi.h>
#include <esp_wifi.h>
#include <PubSubClient.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <env.h>


const byte numChars = 100;
char receivedChars[numChars]; // an array to store the received data
boolean newData = false;

void showNewData();
void recvWithEndMarker();

uint8_t mac[] = {0xB4, 0xE6, 0x2D, 0xB2, 0x1B, 0x36};
WiFiClient wifiClient;
PubSubClient client(wifiClient);

long lastMsg = 0;
char msg[50];
int value = 0;

void wifiSetup() {

  delay(10); 
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  for (int i = 0; i < 6; i++) {
      mac[i] = random(0, 255);
  }

    esp_wifi_set_mac(ESP_IF_WIFI_AP, &mac[0]);


  WiFi.begin(ssid, password);

  WiFi.setAutoReconnect(true);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  

}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "Badge-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // client.publish("scanner", "Connected to MQTT");
      // client.subscribe("scanner");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() { 

  Serial.begin(115200, SERIAL_8N1, 16, 17);

  wifiSetup();

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  Serial.println("Ready");
  Serial.print("Local IP Address: ");
  Serial.println(WiFi.localIP());
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
}

void loop() { 

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
      // client.publish("scanner", "Start");



  // String output = Serial.readStringUntil('\r\n');
  // Serial.println(output);

  recvWithEndMarker();
  showNewData();

}

void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
 
  while (Serial.available() > 0 && newData == false) {
  
    rc = Serial.read();

    if (rc != endMarker) {
      Serial.println("Serial Something");
      receivedChars[ndx] = rc;
      ndx++;
      if (ndx >= numChars) {
        ndx = numChars - 1;
      }
    }
    else {
      receivedChars[ndx] = '\0'; // terminate the string
      ndx = 0;
      newData = true;
    }
  }
}

void showNewData() {
  if (newData == true) {
    client.publish("scanner", receivedChars);
    newData = false;
  }
}